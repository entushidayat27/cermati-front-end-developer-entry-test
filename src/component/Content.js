import React, { Component } from 'react'

class Content extends Component {
    render() {
      return (
        <div className="content">
          <div class="row">
            <div class="col-12">
              <div className="content-title">
                <h1 className="big-title">How Can I Help You?</h1>
                <p className="sub-title">Our work then targeted, best practices outcomes social innovation synergy.<br/> Venture philanthropy, revolutionary inclusive policymaker relief. User-centered program areas scale.</p>
              </div>
            </div>
            <div className="col-4 col-s-6">
              <div className="highlight">
                <h3 className="big-title">Consult <span className="float-right"><i class="fas fa-comments"></i></span></h3>
                <p>Co-create, design thinking; strengthen infrastructure resist granular. Revolution circular, movements or framework social impact low-hanging fruit. Save the world compelling revolutionary progress.</p>
              </div>
            </div>
            <div className="col-4 col-s-6">
              <div className="highlight">
                <h3 className="big-title">Design <span class="float-right"><i class="fas fa-paint-brush"></i></span></h3>
                <p> Policymaker collaborates collective impact humanitarian shared value vocabulary inspire issue outcomes  agile. Overcome injustice deep dive agile issue outcomes vibrant boots on the ground sustainable.</p>
              </div>
            </div>
            <div className="col-4 col-s-6">
              <div className="highlight">
                <h3 className="big-title">Develop <span class="float-right"><i class="fas fa-cubes"></i></span></h3>
                <p>Revolutionary circular, movements a or impact framework social impact low-hanging. Save the compelling revolutionary inspire progress. Collective impacts and challenges for opportunities of thought provoking.</p>
              </div>
            </div>

            <div className="col-4 col-s-6">
              <div className="highlight">
                <h3 className="big-title">Marketing <span className="float-right"><i class="fas fa-bullhorn"></i></span></h3>
                <p>Peaceful; vibrant paradigm, collaborative cities. Shared vocabulary agile, replicable, effective altruism youth. Mobilize commitment to overcome injustice resilient, uplift social transparent effective.</p>
              </div>
            </div>
            <div className="col-4 col-s-6">
              <div className="highlight">
                <h3 className="big-title">Manage <span class="float-right"><i class="fas fa-paint-tasks"></i></span></h3>
                <p> Change-makers innovation or shared unit of analysis. Overcome injustice outcomes strategize vibrant boots on the ground sustainable. Optimism, effective altruism invest optimism corporate social.</p>
              </div>
            </div>
            <div className="col-4 col-s-6">
              <div className="highlight">
                <h3 className="big-title">Evolve <span class="float-right"><i class="fas fa-chart-line"></i></span></h3>
                <p>Activate catalyze and impact contextualize humanitarian. Unit of analysis overcome injustice storytelling altruism. Thought leadership mass incarceration. Outcomes big data, fairness, social game-changer.</p>
              </div>
            </div>
          </div>
        </div>
      );
    }
}

export default Content;