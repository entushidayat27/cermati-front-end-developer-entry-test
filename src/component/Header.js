import React, { Component } from 'react'
import Logo from './Logo'

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            yourname: "Hidayatus Solihin"
        }
    }

    render() {
      return (
          <div>
            <div className="header">
                <div className="row">
                    <Logo />
                    <div className="col-12">
                        <div className="header-title">
                            <h1 className="big-title">Hello! I'm {this.state.yourname}</h1>
                            <h2 className="sub-title">Consult, Design, and Develop Websites</h2>
                            <p className="title">Have something great in mind? Feel free to contact me.<br/> I'll help you to make it happen. Let's Make Contact</p>
                        </div>
                        <button className="cta">Let's Make Contact</button>
                    </div>
                </div>
            </div>

            </div>
      );
    }
}



export default Header;