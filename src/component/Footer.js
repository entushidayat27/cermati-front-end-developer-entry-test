import React, { Component } from 'react'
import Cookies from 'universal-cookie'

const footercookie = new Cookies()

class Footer extends Component {
    constructor(props) {
      super(props);
      this.state = {
          yourname: "Hidayatus Solihin"
      }
    }

    componentDidMount=()=>{
      this.setState({modal : footercookie.get('modalfooter')})
    }
      
    exit=()=>{
      this.setState({modal : false})
      var date = new Date() 
      var time = date.getTime()
      var count = (date.getTime()+(10*60))
      var value = count-time
      footercookie.set('modalfooter',false,{path:"/", maxValue : value})     
    } 

    render() {
      return (
        <div>
          <div className="footer">
            <div class="row">
              <div class="col-12">
                <div className="footer-title">
                  <p>&copy; 2018 {this.state.yourname}. All rights reserved.</p>
                </div>
              </div>
            </div>
          </div>


{this.state.modal===true || this.state.modal===undefined?
<div className="row">
  <div className="col-5 col-s-12 footeralert">
      <div className="icon-exit float-right"><i class="fas fa-times" onClick={this.exit}></i></div>
          <div>
            <span className="footeralertbigtitle">Get latest updates in web tecnologies</span>
            <p className="plaintitle">I write articles related to web technologies, such as design trends, devdelopment tools,
              UI/UX case studies and review, and more. Sign up to my newsletter to get them all.</p>
          </div>
          <div>
            <input type="text" className="form-email" placeholder=" Email address"/>
            <input type="button" className="submit-button" value="Count me in!"/>
          </div> 
      </div>
  </div>
  : null 
}
</div>
      );
    }
}

export default Footer;