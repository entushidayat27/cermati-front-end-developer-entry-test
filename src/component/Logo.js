import React, { Component } from "react"

class Logo extends Component {
    render() {
        return (
            <img className="logo-image" src={require("../asset/img/y-logo-white.png")} alt="Logo" width="90" />
        );
    }
}

export default Logo;