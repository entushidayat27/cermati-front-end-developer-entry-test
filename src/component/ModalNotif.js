import React, { Component } from 'react'
import Cookies from 'universal-cookie'

const modalcookie = new Cookies()

class ModalNotif extends Component{
    state ={modal : true }
     
    componentDidMount=()=>{
        this.setState({modal : modalcookie.get('modal')})
    }
    
    submit=()=>{
        this.setState({modal : false})

        var date = new Date() 
        var time = date.getTime()
        var count = (date.getTime()+(10*60))
        var value = count-time
        modalcookie.set('modal',false,{path:"/", maxValue : value})     
    } 

    render(){
        return(
            <div className="alert-modal">
                {this.state.modal === true || this.state.modal === undefined?
                <div className="alert-content">
                        <div className="alert-title">
                        By accending and using this website, you acknowledge that you have read and<br/> understand our <a href="/"> Cookies Policy, Privacy policy</a> and our <a href ="/"> Terms of Service </a>
                        </div>
                            <input type="button" className="alert-button"  value="Got It !" onClick={this.submit} />
                </div> 
                : null 
            }
            </div>
        )
    }
}

export default ModalNotif;