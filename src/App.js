import React from 'react'
import Header from './component/Header'
import Content from './component/Content'
import Footer from './component/Footer'
import ModalNotif from './component/ModalNotif'
import './asset/css/style.css'

function App() {
  return (
    <div>
      <ModalNotif />
      <Header />
      <Content />
      <Footer />
    </div>
  );
}

export default App;
